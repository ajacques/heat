#!/bin/bash

#lcov --directory . --capture --output-file heat.lcov
#lcov_cobertura.py heat.lcov --output heat-coverage.xml
gcovr -r . -x -o heat-coverage.xml

export DEFINITIONS=""
export CPPCHECK_INCLUDES="-I. -Iinclude"
export SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
export SOURCES_TO_ANALYZE="."

cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml

rats -w 3 --xml ${SOURCES_TO_ANALYZE} > heat-rats.xml

bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.c\|.*\.h" | vera++ - -showrules -nodup |& vera++Report2checkstyleReport.perl > heat-vera.xml'

mpirun "-np" "4" valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes --suppressions=/usr/share/openmpi/openmpi-valgrind.supp --suppressions=tools/heat-valgrind.supp "./build/heat_par" "10" "10" "200" "2" "2" "0"

# Create the config for sonar-scanner
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube
sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat
sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git
sonar.projectKey=sedbso:heat:gitlab:master
sonar.projectName=Heat
sonar.projectDescription=Solve the heat propagation equation
sonar.projectVersion=1.0
sonar.login=`cat ~/.sonarqubetoken`
sonar.scm.disabled=false
sonar.scm.provider=git
sonar.sourceEncoding=UTF-8
sonar.sources=.
sonar.exclusions=build/CMakeFiles/**
sonar.language=c
sonar.c.errorRecoveryEnabled=true
sonar.c.compiler.parser=GCC
sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n')
sonar.c.compiler.charset=UTF-8
sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$
sonar.c.compiler.reportPath=build/heat-build.log
sonar.c.clangsa.reportPath=build/analyzer_reports/*/*.plist
sonar.c.coverage.reportPath=heat-coverage.xml
sonar.c.cppcheck.reportPath=heat-cppcheck.xml
sonar.c.rats.reportPath=heat-rats.xml
sonar.c.vera.reportPath=heat-vera.xml
sonar.c.valgrind.reportPath=heat-valgrind.xml
EOF
