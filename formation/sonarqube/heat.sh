#!/bin/bash

# Download and untar sources
# git command is required
git clone https://gitlab.inria.fr/sed-bso/heat.git
cd heat/

# Configure, Make (with clang-sa analysis using scan-build)
mkdir -p build
cd build
# Optional bloc if you want to clean before building
make clean
export CFLAGS="--coverage -fPIC -fdiagnostics-show-option -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic -g"
export LDFLAGS="--coverage"
cmake .. -DHEAT_USE_MPI=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS"
scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports make 2>&1 | tee heat-build.log

# Execute unitary tests (autotest)
ctest -V

# Collect coverage data
cd ..
lcov --directory . --capture --output-file heat.lcov
genhtml -o coverage heat.lcov
# see the result, for example: chromium-browser coverage/index.html
lcov_cobertura.py heat.lcov --output heat-coverage.xml

# Run cppcheck analysis
export DEFINITIONS=""
export CPPCHECK_INCLUDES="-I."
export SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
export SOURCES_TO_ANALYZE="."
cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml

# Run rats analysis
export SOURCES_TO_ANALYZE="."
rats -w 3 --xml ${SOURCES_TO_ANALYZE} > heat-rats.xml

# Run vera++ analysis
export SOURCES_TO_ANALYZE="."
bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.c\|.*\.h" | vera++ - -showrules -nodup |& vera++Report2checkstyleReport.perl > heat-vera.xml'

# Run valgrind analysis
export OPENMPI_DIR=/usr
$OPENMPI_DIR/bin/mpirun "-np" "4" valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes --suppressions=$OPENMPI_DIR/share/openmpi/openmpi-valgrind.supp --suppressions=tools/heat-valgrind.supp "./build/heat_par" "10" "10" "200" "2" "2" "0"

# Create the config for sonar-scanner
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube
sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat
sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git
sonar.projectKey=sedbso:heat:gitlab:master
sonar.projectName=Heat
sonar.projectDescription=Solve the heat propagation equation
sonar.projectVersion=1.0
sonar.login=`cat ~/.sonarqubetoken`
sonar.scm.disabled=false
sonar.scm.provider=git
sonar.sourceEncoding=UTF-8
sonar.sources=.
sonar.exclusions=build/CMakeFiles/**
sonar.language=c
sonar.c.errorRecoveryEnabled=true
sonar.c.compiler.parser=GCC
sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n')
sonar.c.compiler.charset=UTF-8
sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$
sonar.c.compiler.reportPath=build/heat-build.log
sonar.c.clangsa.reportPath=build/analyzer_reports/*/*.plist
sonar.c.coverage.reportPath=heat-coverage.xml
sonar.c.cppcheck.reportPath=heat-cppcheck.xml
sonar.c.rats.reportPath=heat-rats.xml
sonar.c.vera.reportPath=heat-vera.xml
sonar.c.valgrind.reportPath=heat-valgrind.xml
EOF

# Run the sonar-scanner analysis and submit to SonarQube server
sonar-scanner -X >sonar.log 2>&1
